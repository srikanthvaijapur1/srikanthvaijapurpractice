function countFactory() {

    let counter = 0;

    const increment = (val = 1) => {
        return counter += val;
    }
    const decrement = (val = 1) => {
        return counter -= val;
    }
    return { increment, decrement }
}

const res = countFactory()

console.log(res.increment())

console.log(res.increment(4))

console.log(res.decrement())