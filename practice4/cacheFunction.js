// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {
    let cache = {};

    const invoke = (...parameters) => {
        const key = JSON.stringify(parameters)
        if (cache[key]) {
            console.log('cache',cache)
            return cache[key]
        } else {
            cache[key] = cb(...parameters)
            return cache[key]
        }
    }
    
    return invoke
}

const cb = (a,b)=>{
    return a+b
}

let res = cacheFunction(cb)
console.log(res(2,4))
console.log(res(2,4))

let x = 4
console.log(typeof(x))