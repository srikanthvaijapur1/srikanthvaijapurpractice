let testObject = { name: 'srikanth', age: 26, location: 'karnataka' }

function mapObject(testObject, callback) {
    let obj = {};
    for (let key in testObject) {
        obj[key] = callback(testObject[key])
    }
    return obj;
}

const callback = (key) => {
    return key + '$'
}

const res = mapObject(testObject, callback)
console.log(res)