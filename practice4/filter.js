let array = [1, 2, 3, 4, 5]

function filter(array,callback){
    let res = []
    for(let i = 0 ; i < array.length;i++){
        if(callback(array[i])){
            res.push(array[i])
        }
    }
    return res
}

const callback = (ele)=>{
    return ele>2
}

const res = filter(array,callback)
console.log(res)