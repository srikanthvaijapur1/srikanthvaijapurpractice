let array = [1, 2, 3, 4, 5]

function find(array,callback){
    for(let i = 0 ; i < array.length ; i++){
        if(callback(array[i])){
            return array[i]
        }
    }
}

const callback = (ele)=>{
    return ele >2
}

const res = find(array,callback);
console.log(res)