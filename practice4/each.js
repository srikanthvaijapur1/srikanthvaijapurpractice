let array = [1, 2, 3, 4, 5]

function each(array,callback){
    
    for(let i = 0 ; i < array.length;i++){
        callback(array[i])
    }
    return;
}

const callback = (element)=>{
    console.log(element*4)
}

let res = each(array,callback)

