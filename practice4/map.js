let array = [1, 2, 3, 4, 5]

function map(array,callback){
    let res = []
    for(let i = 0 ; i < array.length ; i++){
        res.push(callback(array[i]));
    }
    return res;
}

const callback = (ele)=>{
    return ele*2
}

const res = map(array,callback);
console.log(res);