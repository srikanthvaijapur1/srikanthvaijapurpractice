const array = require("../array");

function filter(array,callback){
    let result = [];
    for(let index = 0 ; index < array.length ; index++){
        if(callback(array[index])){
            result.push(array[index])
        }
    }
    return result;
}

function callback(element){
    return element>2;
}

let result =  filter(array,callback);
console.log(result)