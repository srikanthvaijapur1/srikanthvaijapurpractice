const array = require("../array");

function map(array, callback) {
    let res = [];
    for (let index = 0; index < array.length; index++) {
        res.push(callback(array[index]))
    }
    return res
}

function callback(element) {
    return element - 2
}

let result = map(array,callback)
console.log(result);