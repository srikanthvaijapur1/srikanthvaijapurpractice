const array = require("../array");

function reduce(array, callback, startingValue) {
    if (!startingValue) {
        startingValue = 0
    }
    for (let index = 0; index < array.length; index++) {
        startingValue = callback(startingValue, array[index])
    }
    return startingValue
}

const callback = (accumulator, currentValue) => {
    return accumulator + currentValue
}

let result = reduce(array,callback);

console.log(result)