const array = require("../array")

function find(array,callback){
    for(let index = 0 ; index < array.length ; index++){
        if(callback(array[index])){
            return array[index]
        }
    }
}

function callback(element){
    return element>3
}

let result  =  find(array,callback);
console.log(result)