const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function invert(obj){
    let res = {}
    for(let key in obj){
        res[obj[key]]=key
    }
    return res
}

let result =  invert(testObject)
console.log(result)