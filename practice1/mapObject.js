let testObject = { name: 'srikanth', age: 26, location: 'karnataka' }

function mapObj(obj, callback) {
    let map = {}
    for (let key in obj) {
        map[key]=callback(obj[key])
    }
    return map
}

function callback(obj){
    return obj + '%$%'
}

let result = mapObj(testObject,callback)
console.log(result)