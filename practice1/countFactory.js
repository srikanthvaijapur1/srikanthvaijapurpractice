function countFactory() {
    let counter = 0;
    const increment = (value = 1) => {
        return counter += value
    }
    const decrement = (value = 1) => {
        return counter -= value
    }
    return { increment, decrement }
}


const result = countFactory();
console.log(result.increment())
console.log(result.increment())
console.log(result.increment(4))
console.log(result.decrement())