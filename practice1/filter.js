let array = require("../array")

function filter(array, callback) {
    let res = [];
    for (let index = 0; index < array.length; index++) {
        if (callback(array[index])) {
            res.push(array[index])
        }

    }
    return res
}

function callback(element) {
    return element > 2
}

let result = filter(array, callback)
console.log(result)