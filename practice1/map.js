// let array = [1, 2, 3, 4, 5]
let array = require("../array")

function map(array, callback) {
    let res = [];
    for (let index = 0; index < array.length; index++) {
        res.push(callback(array[index]))
    }
    return res
}

function callback(ele) {
    return ele * 2
}

let result = map(array, callback)

console.log(result)