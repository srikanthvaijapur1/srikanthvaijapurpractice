function limiFunctionCallCount(callback,n){
    let counter = 0;
    let excute = (...array)=>{
        if(counter<n){
            counter++
            return callback(...array)
        }
        return null
    }
    return excute
}

const callback = (a,b)=>a*b
let result = limiFunctionCallCount(callback,2)
console.log(result(2,4))
console.log(result(2,4))
console.log(result(2,4))