// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {
    const cache = {}
    if (cb == undefined) return null

    const invoke = (...parameters) => {
        const key = JSON.stringify(parameters)
        if(cache[key]) return cache[key]
        cache[key]= cb(...parameters)
        return cache[key]
    }
    return invoke

}


const cb = (a,b)=>{
    console.log('abcd')
    return a+b

}
let result = cacheFunction(cb)
console.log(result(2,3))