function countFactory() {
    let count = 0;
    const increment = (val = 1) => {
        return count += val
    }
    const decerement = (val=1) => {
        return count -= val
    }
    return { increment, decerement }
}

let res = countFactory()

console.log(res.increment())
console.log(res.increment())
console.log(res.increment(2))
console.log(res.decerement())