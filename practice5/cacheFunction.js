function cacheFunction(cb) {
    let cache = {}

    const invoke = (...parameters) => {
        const key = JSON.stringify(parameters)
        if (cache[key]) {
            console.log('from cacche',cache[key])
            return cache[key]
        } else {
            cache[key] = cb(...parameters)
            return cache[key]
        }
    }
    return invoke;
}

let cb = (a,b)=>{
    return a*b
}

let res = cacheFunction(cb)

console.log(res(2,3))
console.log(res(2,3))