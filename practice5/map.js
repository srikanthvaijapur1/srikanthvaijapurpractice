let array = [1, 2, 3, 4, 5]

const map = (array, cb) => {
    let res = [];
    for (let index = 0; index < array.length; index++) {
        console.log(cb(array[index]))
        res.push(cb(array[index]))
    }
    return res
}

const cb = (ele) => { return ele * 2 }

let res = map(array, cb);

console.log(res)