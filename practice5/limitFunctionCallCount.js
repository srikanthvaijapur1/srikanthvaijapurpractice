function limitFunctionCallCount(cb, n) {
    let count = 0;

    const excute = (...parameters) => {
        if (count < n) {
            count++
            return cb(...parameters)
        }else{
            return null
        }
    }
    return excute;
}

const cb=(a,b)=>{
    return a+b
}
let res = limitFunctionCallCount(cb,2);
console.log(res(2,3))
console.log(res(2,3))
console.log(res(2,3))