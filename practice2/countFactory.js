// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.

function counterFactory() {
    let counter = 0;
    const increment = (val = 1) => {
        return counter += val
    }
    const decrement = (val = 1) => {
        return counter -= val
    }

    return { increment, decrement }
}

let result = counterFactory()
console.log(result.increment())
console.log(result.increment(2))
console.log(result.decrement())
