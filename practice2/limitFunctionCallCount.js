// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
function limitFunctionCallCount(cb, n) {
    let counter = 0;
    const excute = (...parameters) => {
        if (counter < n) {
            counter++
            return cb(...parameters)
        }
        return null
    }
    return excute
}

const cb = (a, b) => {
    return a * b
}

const result = limitFunctionCallCount(cb, 2)
console.log(result(2, 3))
console.log(result(2, 3))
console.log(result(2, 3))